import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Animal} from "./types/animal";
import {Observable, Subject} from "rxjs";

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) {
  }

  getAnimals(): Observable<Animal[]> {
    return this.http.get<Animal[]>('http://localhost:3000/animals')
  }

  findAnimalById(id: number) {
    return this.http.get<Animal>(`http://localhost:3000/animals/${id}`);
  }

  updateAnimalById(animal: Animal) {
    return this.http.put(`http://localhost:3000/animals/${animal.id}`, animal);
  }

  deleteAnimalById(id: number) {
    return this.http.delete(`http://localhost:3000/animals/${id}`)
  }

  createAnimal(animal: Animal) {
    return this.http.post(`http://localhost:3000/animals`, animal)
  }

  newSubject = new Subject();
}
