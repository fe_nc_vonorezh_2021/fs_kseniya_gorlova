import {Injectable} from "@angular/core";
import {FamilyEnum} from "../enum/family.enum";
import {Animal} from "../types/animal";

@Injectable()
export class AnimalsService {

  _animals: Animal[] = [
    { name: 'Муся', id: 1, type: 'Кошка', sex: 'ж', color: 'рыжая', age: 1, family: FamilyEnum.Cats, uniqueProperties: 'быстро бегать за мышкой'},
    { name: 'Дружок', id: 2, type: 'Пёс', sex: 'м', color: 'белый с чёрными пятнами', age: 1, family: FamilyEnum.Dogs, uniqueProperties: 'гоняться за птицами'},
    {name: 'Хедвик', id: 3, type: 'Сова', sex: 'м', color: 'серый', age: 1, family: FamilyEnum.Birds, uniqueProperties: 'доставлять почту'},
    {name: 'Фоукс', id: 4, type: 'Феникс', sex: 'м', color: 'оранжевый',age:  101, family: FamilyEnum.Birds, uniqueProperties: 'исцелять слезами, переносить вес в несколько раз больше себя'},
    {name: 'Симба', id: 5, type: 'Лев',sex:  'м', color: 'жёлтый', age: 1, family: FamilyEnum.Cats, uniqueProperties: 'честность'},
    {name: 'Гав', id: 6, type: 'Котёнок', sex: 'м', color: 'песочный', age: 1, family: FamilyEnum.Cats, uniqueProperties: 'искать неприятности'},
    {name: 'Плотва', id: 7, type: 'Лошадь', sex: 'ж', color: 'коричневая', age: 1, family: FamilyEnum.Equine, uniqueProperties: 'понимает своего хозяина'},
    {name: 'Немо', id: 8, type: 'Рыбка', sex: 'м', color: 'оранжевая', age: 1, family: FamilyEnum.Fish, uniqueProperties: 'красиво плавает'}
  ];

  constructor() { }

  getList(): Animal[] {
    return this._animals;
  }

  getFilteredList(): Animal[] {
    return this._animals.filter(animal => animal.family !== FamilyEnum.Cats);
  }

}
