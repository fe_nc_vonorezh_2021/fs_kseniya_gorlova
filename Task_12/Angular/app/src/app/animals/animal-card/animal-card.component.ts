import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {AnimalsService} from "../../service/animals.service";
import {Animal} from "../../types/animal";


@Component({
  selector: 'animal-card',
  templateUrl: './animal-card.component.html',
  styleUrls: ['./animal-card.component.less'],
  providers: [
    AnimalsService
  ]
})
export class AnimalCardComponent implements OnInit {
  title = 'animal-card';

  @Input()
  animal: Animal;

  @Input()
  active: boolean;

  @Output()
  onAnimalSelect: EventEmitter<number> = new EventEmitter();

  onClick() {
    this.onAnimalSelect.emit(this.animal.id);
  }

  ngOnInit(): void {
    console.log(this.animal);
  }
}
