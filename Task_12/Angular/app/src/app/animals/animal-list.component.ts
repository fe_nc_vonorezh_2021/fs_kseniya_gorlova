import {Component, Input, OnInit, Output} from "@angular/core";
import {AnimalsService} from "../service/animals.service";
import {AnimalCardComponent} from "./animal-card/animal-card.component";
import {Animal} from "../types/animal";

@Component({
  selector: 'animals',
  templateUrl: './animal-list.component.html',
  styleUrls: ['./animal-list.component.less'],
  providers: [
    AnimalsService,
    AnimalCardComponent
  ]
})
export class AnimalListComponent implements OnInit {
  title = 'animal';
  isCatsShown = true;
  _animals: Animal[] = [];
  _activeId: number = 0;

  constructor(
    private service: AnimalsService
  ) {}

  ngOnInit(): void {
    this._animals = this.service.getList();
  }

  toggleCatsVisibility() {
    this.isCatsShown = !this.isCatsShown;
    this._animals = this.isCatsShown ? this.service.getList() : this.service.getFilteredList();
  }

  activeAnimal(id: number) {
    this._activeId = id;
  }
}
