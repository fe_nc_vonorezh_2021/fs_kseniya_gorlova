import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {AnimalCardComponent} from "./animal-card/animal-card.component";
import {AnimalListComponent} from "./animal-list.component";

@NgModule({
  declarations: [
    AnimalListComponent,
    AnimalCardComponent
  ],
  imports: [
    BrowserModule
  ],
  exports: [
    AnimalListComponent
  ],
  providers: []
})
export class AnimalModule { }
