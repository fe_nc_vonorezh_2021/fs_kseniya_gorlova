export enum FamilyEnum {
  Cats,
  Dogs,
  Birds,
  Equine,
  Fish
}
