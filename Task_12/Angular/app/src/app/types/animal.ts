import {FamilyEnum} from "../enum/family.enum";

export type Animal = {
  name: string;
  id: number;
  type: string;
  sex: string;
  color: string;
  age: number;
  family: FamilyEnum;
  uniqueProperties: string;
};
