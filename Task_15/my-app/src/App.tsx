import React, {useMemo, useState} from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import AnimalList from "./components/ListAnimals/AnimalList";
import Animal from "./components/Animal/Animal";
import ThemeContext from "./ThemeContext";
import Header from "./components/Header/Header";
import style from './App.module.css';
import {themes} from "./utils/utils";

const App: React.FC = () => {
    const [isDark, setIsDark] = useState(false);

    const context = useMemo(() => ({
        isDark,
        toggleTheme: () => setIsDark(prev => !prev)
    }), [isDark]);

    const s = isDark ? themes.dark : themes.light;

    return (
        <BrowserRouter>
            <ThemeContext.Provider value={context}>
                <div className={style.container} style={{backgroundColor: s.background}}>
                    <Header/>
                    <Routes>
                        <Route path='/' element={<AnimalList/>}/>
                        <Route path='/animal/:id' element={<Animal/>}/>
                    </Routes>
                </div>
            </ThemeContext.Provider>
        </BrowserRouter>
    );
}

export default App;
