export type ButtonProps = {
    onClick: () => void,
    value: string
}