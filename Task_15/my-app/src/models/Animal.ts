export type AnimalType = {
    name: string;
    id: number;
    type: string;
    sex: string;
    color: string;
    age: number;
    family: string;
    uniqueAbilities: string;
};