export const url = process.env.REACT_APP_BASE_URL || '';

export const themes = {
    light: {
        color: "#161D6F",
        background: "#F4F9F9",
        buttonColor: "#CCF2F4",
    },
    dark: {
        color: "#BBE1FA",
        background: "#0F4C75",
        buttonColor: "#3282B8",
    }
};
