import React, {useContext} from "react";
import style from "./Button.module.css";
import ThemeContext from "../../ThemeContext";
import {ButtonProps} from "../../models/Button";
import {themes} from "../../utils/utils";

const Button: React.FC<ButtonProps> = props => {
    const context = useContext(ThemeContext);
    const themeStyle = context.isDark ? themes.dark : themes.light

    return (
    <button className={style.button}
            style={{backgroundColor: themeStyle.buttonColor, color: themeStyle.color}}
            onClick={props.onClick}>{props.value}</button>
    );
};

export default React.memo(Button);