import React, {useContext} from "react";
import ThemeContext from "../../ThemeContext";
import style from "./Header.module.css";
import {themes} from "../../utils/utils";

const Header: React.FC = () => {
    const context = useContext(ThemeContext);
    const themeStyle = context.isDark ? themes.dark : themes.light;

    return (
        <header className={style.header} style={{backgroundColor: themeStyle.background}}>
            <button style={{backgroundColor:themeStyle.buttonColor, color: themeStyle.color}}
                    className={style.button}
                    onClick={() => context.toggleTheme()}>{context.isDark ? 'Светлая' : 'Тёмная'} тема</button>
            <h1 style={{color: themeStyle.color}}>Зоомагазин</h1>
        </header>
    );
}

export default Header;