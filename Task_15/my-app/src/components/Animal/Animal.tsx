import React, {useContext, useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import ThemeContext from "../../ThemeContext";
import style from "./Animal.module.css";
import {themes, url} from "../../utils/utils";
import {AnimalType} from "../../models/Animal";

const Animal: React.FC = () => {
    const [animal, setAnimal] = useState<AnimalType | null>(null);
    const params = useParams()
    const context = useContext(ThemeContext);
    const themeStyle = context.isDark ? themes.dark : themes.light;

    useEffect(() => {
        fetch(`${url}/${params.id}`)
            .then(response => response.json())
            .then(data => setAnimal(data))
    }, [])

    return (
        <div className={style.info}
             style={{backgroundColor: themeStyle.background, color: themeStyle.color}}>
            {animal ?
                <div className={style.card}>
                    <h4>{animal.type} {animal.name}.</h4>
                    <p>Возраст: {animal.age} год;</p>
                    <p>Пол животного: {animal.sex};</p>
                    <p>Расцветка: {animal.color};</p>
                    <p>Уникальные способности: {animal.uniqueAbilities}.</p>
                </div>
                :
                <></>}
        </div>
    )
}

export default Animal;