import React, {useCallback, useContext, useEffect, useState} from "react";
import {NavLink} from "react-router-dom";
import ThemeContext from "../../ThemeContext";
import style from "./AnimalList.module.css";
import Button from "../Button/Button";
import {AnimalType} from "../../models/Animal";
import {themes, url} from "../../utils/utils";

const AnimalList = () => {
    const [animals, setAnimals] = useState<AnimalType[]>([]);
    const [isCatsShown, showCats] = useState<boolean>(true);
    const context = useContext(ThemeContext);
    const themeStyle = context.isDark ? themes.dark : themes.light;

    useEffect(() => {
        let cleanUp = false;

        fetch(url)
            .then(response => response.json())
            .then(data => !cleanUp ? setAnimals(data) : undefined)
            .catch(err => console.error(err));

        return () => {
            cleanUp = true;
        }
    }, []);
    const handleClick = useCallback(() => showCats(prevState => !prevState), []);

    return (
        <div className={style.animals} style={{backgroundColor: themeStyle.background}}>
            <h2 style={{color: themeStyle.color}}>Список животных:</h2>
            <ul>{
                animals
                    .filter(item => isCatsShown ? true : item.family !== 'Cats')
                    .map(animal =>
                        <li key={animal.id} style={{borderColor: themeStyle.color}}>
                            <NavLink to={`/animal/${animal.id}`} style={{color: themeStyle.color}}>
                                <div>
                                    <h4>{animal.type} {animal.name}</h4>
                                </div>
                            </NavLink>
                        </li>)}
            </ul>
            <Button onClick={handleClick}
                    value={isCatsShown ? "Скрыть котиков" : "Показать котиков"} />
        </div>

    );
}

export default AnimalList;