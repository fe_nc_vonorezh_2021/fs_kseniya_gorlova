const regExpPhone = /^\+?[78][-(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$/;
const regExpEmail = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
const regExpWhiteSpace = /^\s+/;
const regExpIsEmptyLine = /^\s*$/;

const inputPhone = document.querySelector('.phone');
const inputLastName = document.querySelector('.last-name');
const inputFirstName = document.querySelector('.first-name');
const inputEmail = document.querySelector('.email');
const inputMessage = document.querySelector('.message');
const errorMessage = document.querySelector('.error-message');
const btnSend = document.querySelector('.btn-send');
const checkBoxAcceptationUser = document.querySelector('.custom-checkbox')
const requiredInputs = [inputEmail, inputLastName, inputMessage, inputFirstName];
const allInputs = [inputEmail, inputPhone, inputLastName, inputMessage, inputFirstName];

checkBoxAcceptationUser.addEventListener('change', () => btnSend.disabled = hasEmptyField())

document.querySelector('.btn-send').onclick = function (e) {
    e.preventDefault();
    const isPhoneValid = checkValidity(regExpPhone, inputPhone.value);
    const isEmailValid = checkValidity(regExpEmail, inputEmail.value);

    if (inputPhone.value !== '' && !isPhoneValid && !isEmailValid) {
        addError(inputPhone, '');
        addError(inputEmail, 'Телефон и email введены некорректно');
    } else if (!isEmailValid) {
        addError(inputEmail, 'Email введен некорректно');
    } else if (inputPhone.value !== '' && !isPhoneValid) {
        addError(inputPhone, 'Телефон введен некорректно');
    } else {
        clearError(inputEmail);
        clearError(inputPhone);
        submitForm();
    }
};

function submitForm() {
    const cookies = getCookies();

    if (cookies && cookies.sent === 'true'
                && cookies.firstName === `${inputFirstName.value}`
                && cookies.lastName === `${inputLastName.value}`) {
        alert(`${inputFirstName.value} ${inputLastName.value} ваше обращение уже обрабатывается!`);
        return;
    }

    document.cookie = 'sent=true';
    document.cookie = `firstName=${inputFirstName.value}`;
    document.cookie = `lastName=${inputLastName.value}`;
    alert(`${inputFirstName.value} ${inputLastName.value} спасибо за обращение!`);
    document.querySelector('.request-form').reset();
    btnSend.disabled = hasEmptyField();
}

function getCookies() {
    return document.cookie.split('; ').reduce((res, item) => {
        const arr = item.split('=')
        res[arr[0]]=arr[1];
        return res;
    }, {})
}

allInputs.forEach(el => {
    el.addEventListener('keyup', () => {
        localStorage.setItem(`${el.name}`, el.value);
        btnSend.disabled = hasEmptyField();
    });
});

requiredInputs.forEach(el => {
    el.addEventListener( 'keyup', () => {
        addValidStyle(el);
    })
});

function hasEmptyField() {
    const hasEmpty = requiredInputs.some(el => checkValidity(regExpIsEmptyLine, el.value) || checkValidity(regExpWhiteSpace, el.value));
    const checked = checkBoxAcceptationUser.checked;
    return hasEmpty || !checked;
}

function checkValidity(regex, value) {
    return regex.test(value);
}

function clearError(input) {
    input.classList.remove('is-invalid');
    errorMessage.innerHTML = '';
}

function addError(input, message) {
    input.classList.add('is-invalid');
    errorMessage.innerHTML = message;
}

function addValidStyle(el) {
    if (checkValidity(regExpIsEmptyLine, el.value) || checkValidity(regExpWhiteSpace, el.value)) {
        el.classList.add('is-invalid');
    } else {
        el.classList.remove('is-invalid');
    }
}

allInputs.forEach(el => {
    el.value = localStorage.getItem(`${el.name}`);
});

const disabled = hasEmptyField();
btnSend.disabled = disabled;
