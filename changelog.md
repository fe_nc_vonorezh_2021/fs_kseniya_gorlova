| Дата          | Название        | Описание      | Ссылка на MR |
| ------------  |---------------  | ------------- | ------------ |
| 13.10.2021    | Task 1          | add readme    | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/1   |
| 19.10.2021    | Task 2          | add side Travel | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/2 | 
| 26.10.2021    | Task 4          | add Task 4    | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/3   |
| 04.11.2021    | Task 5          | add Task 5    | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/4   |
| 09.11.2021    | Task 6          | add Task 6    | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/5   |
| 11.11.2021    | Task 7          | add Task 7    | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/6   |
| 16.11.2021    | Task 8          | add Task 8    | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/7   |
| 19.11.2021    | Task 9          | add Task 9    | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/8   |
| 25.11.2021    | Task 10         | add Task 10   | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/9   |
| 08.12.2021    | Task 11         | add Task 11   | https://gitlab.com/fe_nc_vonorezh_2021/fs_kseniya_gorlova/-/merge_requests/10  |