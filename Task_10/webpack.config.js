const path = require("path");

module.exports = {
    entry: './src/rxjs/3/index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    }
}