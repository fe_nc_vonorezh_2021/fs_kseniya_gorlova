class Node {
    constructor({value, prev = null, next = null}) {
        this.value = value;
        this.prev = prev;
        this.next = next;
    }
}

class LinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this._size = 0;
    }

    size() {
        return this._size;
    }

    append(value) {
        const node = new Node({value});

        if (this.tail) {
            this.tail.next = node;
        }

        if (!this.head) {
            this.head = node;
        }

        node.value = value;
        node.prev = this.tail;
        this.tail = node;
        this._size++;
    }

    add(value, index) {
        const found = this.find(index);

        if (!found) {
            console.log(`Невозможно добавить элемент по индексу - ${index}`);
            return null;
        }

        const node = new Node({value});
        const prev = found.prev;
        node.prev = prev;

        if (prev !== null) {
            prev.next = node;
        }
        found.prev = node;
        node.next = found;

        if (this.head === found) {
            this.head = node;
        }
        this._size++;
    }

    find(index) {
        if (!this.head) {
            console.log('список пустой');
            return null;
        } else if (this._size < index) {
            return null;
        } else if (index > this._size / 2) {
            let current = this.tail;
            let i = this._size - 1;
            while (current) {
                if (i === index) {
                    return current;
                }
                current = current.prev;
                i--;
            }
        } else {
            let current = this.head;
            let i = 0;
            while (current) {
                if (i === index) {
                    return current
                }
                current = current.next;
                i++;
            }
        }
        return null;
    }

    delete(value) {
        if (!this.head) {
            return console.log('список пуст');
        }

        let current = this.head;

        if (current.value === value) {
            const next = current.next;
            next.prev = null;
            this.head = next;
        } else if (this.tail.value === value) {
            const prev = this.tail.prev;
            prev.next = null;
            this.tail = prev;
        } else {
            while (current.next) {
                current = current.next;

                if (current.value === value) {
                    const next = current.next;
                    const prev = current.prev;
                    next.prev = prev;
                    prev.next = next;
                }
            }
        }
        this._size--;
    }

    get(index) {
        const current = this.find(index);
        return current.value;
    }

    edit(index, newValue) {
        const current = this.find(index);

        if (current) {
            current.value = newValue;
        } else {
            console.log(`Невозможно исправить элемент пои индексу - ${index}`)
            return null;
        }
    }

    toArray() {
        const list = [];
        let current = this.head;

        while (current) {
            list.push(current.value);
            current = current.next;
        }

        return list;
    }
}

const list = new LinkedList();
list.append('сегодня');
list.append('завтра');
list.append('всегда');
list.delete('завтра');
console.log(list.toArray())
list.add('завтра', 1);
console.log(list.toArray())
list.add('никогда', 3);
console.log(list.toArray())
const first = list.get(0);
console.log(`Элемент в списке по индексу "0" имеет значение "${first}"`)
list.edit(0, 'не сегодня')
console.log(list.toArray())
list.add('test', 566);
list.edit(4,'когда?');

