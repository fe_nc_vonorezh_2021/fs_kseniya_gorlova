const {fromEvent, merge} = require("rxjs");

const btn_test1 = document.querySelector('.btn-test1');
const btn_test2 = document.querySelector('.btn-test2');
const btn_test3 = document.querySelector('.btn-test3');

const stream_one$ = fromEvent(btn_test1,'click');
const stream_two$ = fromEvent(btn_test2,'click');
const stream_tree$ = fromEvent(btn_test3,'click');

merge(stream_one$, stream_two$, stream_tree$).subscribe(() => {
    document.body.style.background = '#' + (Math.random().toString(16) + '000000').substring(2,8).toUpperCase();
})
