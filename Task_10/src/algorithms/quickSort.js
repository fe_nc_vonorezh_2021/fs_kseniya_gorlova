function getRandomArr() {
    return [...Array(20)].map(() => Math.floor(Math.random() * 101));
}

const arr = getRandomArr();
console.log(arr);

function quickSort(arr) {
    if (arr.length < 2) {
        return arr;
    } else {
        const supportElement = arr[Math.round(arr.length / 2) - 1];
        const left = arr.filter(el => {
            const flag = el === supportElement;
            return !flag && (el <= supportElement);
        });

        const right = arr.filter(el => el > supportElement);

        return [...quickSort(left), supportElement, ...quickSort(right)];
    }
}

console.log(quickSort(arr));
