import {FamilyEnum} from "../enum/family.enum";

export class Animal {

  private _name: string;
  private _id: number;
  private _type: string;
  private _sex: string;
  private _color: string;
  private _age: number;
  private _family: FamilyEnum;
  private _uniqueProperties: string;

  constructor(name: string,
              id: number,
              type: string,
              sex: string,
              color: string,
              age: number,
              family: FamilyEnum,
              uniqueProperties: string) {
    this._name = name;
    this._id = id;
    this._type = type;
    this._sex = sex;
    this._color = color;
    this._age = age;
    this._family = family;
    this._uniqueProperties = uniqueProperties;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  get sex(): string {
    return this._sex;
  }

  set sex(value: string) {
    this._sex = value;
  }

  get color(): string {
    return this._color;
  }

  set color(value: string) {
    this._color = value;
  }

  get age(): number {
    return this._age;
  }

  set age(value: number) {
    this._age = value;
  }

  get family(): FamilyEnum {
    return this._family;
  }

  set family(value: FamilyEnum) {
    this._family = value;
  }

  get uniqueProperties(): string {
    return this._uniqueProperties;
  }

  set uniqueProperties(value: string) {
    this._uniqueProperties = value;
  }

  toHtml() {
    return `${this._type} ${this._name}`;
  }
}
