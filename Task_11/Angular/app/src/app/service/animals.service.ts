import {Injectable} from "@angular/core";
import {Animal} from "../animals/animal";
import {FamilyEnum} from "../enum/family.enum";

@Injectable()
export class AnimalsService {

  private animals: Animal[] = [
    new Animal('Муся', 1, 'Кошка', 'ж', 'рыжая', 1, FamilyEnum.Cats, 'быстро бегать за мышкой'),
    new Animal('Дружок',2, 'Пёс', 'м', 'белый с чёрными пятнами', 1, FamilyEnum.Dogs, 'гоняться за птицами'),
    new Animal('Хедвик', 3, 'Сова', 'м', 'серый', 1, FamilyEnum.Birds, 'доставлять почту'),
    new Animal('Фоукс', 4, 'Феникс', 'м', 'оранжевый', 101, FamilyEnum.Birds, 'исцелять слезами, переносить вес в несколько раз больше себя'),
    new Animal('Симба', 5, 'Лев', 'м', 'жёлтый', 1, FamilyEnum.Cats, 'честность'),
    new Animal('Гав', 6, 'Котёнок', 'м', 'песочный', 1, FamilyEnum.Cats, 'искать неприятности'),
    new Animal('Плотва', 7,'Лошадь', 'ж', 'коричневая', 1, FamilyEnum.Equine, 'понимает своего хозяина'),
    new Animal('Немо', 8, 'Рыбка', 'м', 'оранжевая', 1, FamilyEnum.Fish, 'красиво плавает')
  ];

  constructor() { }

  getList(): Animal[] {
    return this.animals;
  }

  getFilteredList(): Animal[] {
    return this.animals.filter(animal => animal.family !== FamilyEnum.Cats);
  }

}
