import { Component } from '@angular/core';
import {AnimalsService} from "./service/animals.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    AnimalsService
  ]
})
export class AppComponent {
  title = 'app';
  isCatsShown = true;
  animals = this.service.getList();
  activeIdArr: number[] = [];

  constructor(private service: AnimalsService) {

  }

  toggleCatsVisibility() {
    this.isCatsShown = !this.isCatsShown;
    this.animals = this.isCatsShown ? this.service.getList() : this.service.getFilteredList();
  }

  activeAnimal(id: number) {
    this.activeIdArr.push(id);
  }

  isActive(animalId: number) {
    return !!this.activeIdArr.find(item => item === animalId);
  }
}
