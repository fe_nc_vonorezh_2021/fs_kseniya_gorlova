const history = document.querySelector('.history ul');
const input = document.querySelector('.result');
const point = document.querySelector('button[data-type="point"]');
const errorMessage = 'Введите данные корректно';
const square = toPow(2);
let lastResult = '';

document.querySelector('.form').addEventListener('click', logic);

function getResult(res) {
    try {
        return eval(res);
    } catch {
        return 'error'
    }
}
const writeResult = (res, exp) => {
    if (isNaN(res) || res === Infinity || res === 'error') {
        input.textContent = 'Введите данные корректно';
        return;
    }

    point.disabled = !Number.isInteger(res);
    input.textContent = res;
    const li = document.createElement('li');
    li.innerHTML = `${exp} = ${res}`;
    history.append(li);
    lastResult = res;
}

function calculate() {
    let exp = input.textContent;

    if (exp.includes('^')) {
        const replaced = exp.replace('^', '**');
        writeResult(getResult(replaced), exp);
    } else {
        writeResult(getResult(exp), exp);
    }
}

function toPow(pow) {
    return function (num) {
        return num ** pow;
    }
}

function squareLogic() {
    let value = input.textContent;
    let res = square(Number(value));
    writeResult(res, `${value}²`);
}

function backspace() {
    let value = input.textContent;
    if (value.length > 0) {
        if (value[value.length - 1] === '.') {
            point.disabled = false;
        }
        value = value.substring(0, value.length - 1);
        input.textContent = value;
        lastResult = '';
    }
}

function clearStr() {
    input.textContent = '';
    point.disabled = false;
    lastResult = '';
}

function sqrt() {
    let text = input.textContent;
    let res = Math.sqrt(Number(text));
    writeResult(res, `√${text}`);
}

function addPoint() {
    if (isOperation(input.textContent[input.textContent.length - 1]) || input.textContent.length === 0) {
        input.textContent += '0.';
    } else {
        input.textContent += '.';
    }

    point.disabled = true;
}

function operationLogic(event) {
    const operation = event.target.textContent;
    const lastChar = input.textContent[input.textContent.length - 1];

    if (isOperation(lastChar)) {
        if (operation !== lastChar) {
            input.textContent = input.textContent.slice(0, -1) + operation;
        }
    } else {
        input.textContent += operation;
    }

    point.disabled = false;
}

function isOperation(char) {
    return '+-*/()^'.includes(char);
}

function numberLogic(event) {
    const text = event.target.textContent;

    if (lastResult !== '' && input.textContent === lastResult.toString()) {
        input.textContent = text;
        lastResult = '';
        point.disabled = false;
    } else if (input.textContent === '0' || input.textContent === errorMessage) {
        input.textContent = text;
    } else {
        input.textContent += text;
    }
}

const logicMap = {
    operation: operationLogic,
    number: numberLogic,
    calculation: calculate,
    backspace: backspace,
    clear: clearStr,
    operationSqrt: sqrt,
    operationPow: squareLogic,
    point: addPoint
}

function logic(event) {
    const {type} = event.target.dataset;

    if (!type) {
        return;
    }

    logicMap[type](event);
}
