class Person {

    constructor({name, sex, age}) {
        this._name = name;
        this._sex = sex;
        this._age = age;
    }

    go() {
        console.log(`${this.name} гуляет...`)
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get sex() {
        return this._sex;
    }

    set sex(newSex) {
        this._sex = newSex;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    // eat(dish) {
    //     console.log(`${this.name} кушает ${dish}`)
    // }

    toString() {
        return this.name;
    }
}

class Prof extends Person {
    constructor({name, sex, age, courseName}) {
        super({name, sex, age});
        this.courseName = courseName;
    }

    teachStudents() {
        console.log(`${this.name} учит студентов предмету ${this.courseName}...`)
    }
}

class Student extends Person {
    constructor(person) {
        super(person);
        this._education = person.education;
    }

    set education(newEducation) {
        this._education = newEducation;
    }

    get education() {
        return this._education;
    }

    protectionOfTheDiploma() {
        return this._education = 'Высшее';
    }

    toString() {
        return `${this.name}, пол: ${this.sex}, образование: ${this.education}`
    }
}

class Group {
    constructor({name, curator, students}) {
        this._name = name;
        this._curator = curator;
        this._students = students;
    }

    get name() {
        return this._name;
    }

    get curator() {
        return this._curator;
    }

    set name(newName) {
        this._name = newName;
    }

    set curator(newCurator) {
        this._curator = newCurator;
    }

    get studentsString() {
        return this.students.reduce((res, student) => {
            return `${res}\n${student.toString()}`
        }, '')
    }

    get students() {
        return this._students;
    }

    set students(newStudents) {
        this._students = newStudents;
    }
}

class Faculty {
    constructor({name, dean, groups, professors}) {
        this._name = name;
        this._dean = dean;
        this._groups = groups;
        this._professors = professors;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get studentsCount() {
        return this._groups.reduce((sum, group) => {
            return sum + group.students.length
        }, 0)
    }

    set professors(newProfessor) {
        this._professors = newProfessor;
    }

    get professors() {
        return this._professors;
    }

    get groups() {
        return this._groups;
    }

    set groups(newGroups) {
        this._groups = newGroups;
    }

    dismiss(nameProfessor) {
        console.log(`Профессор ${nameProfessor} был уволен деканом ${this._dean}`);
        this.professors = this.professors.filter(prof => prof.name !== nameProfessor);
    }

    expulsion(studentName, groupName) {
        console.log(`Студент ${studentName} был отчислен деканом ${this._dean}`);
        const group = this.groups.find(group => group.name === groupName);
        group.students = group.students.filter(student => student.name !== studentName);
    }
}

const groupPhysicists = [
    new Student({name: 'Горлова Ксения', sex: 'ж', age: 22, education: 'Среднее'}),
    new Student({name: 'Сидоров Иван', sex: 'м', age: 21, education: 'Среднее'}),
    new Student({name: 'Гаврилов Степан', sex: 'м', age: 22, education: 'Среднее'}),
    new Student({name: 'Соболева Валентина', sex: 'ж', age: 21, education: 'Среднее'}),
    new Student({name: 'Смирнов Александр', sex: 'м', age: 22, education: 'Среднее'})
]
const st1 = new Student({name: 'Сидоренко Алевтина', sex: 'ж', age: 22, education: 'Среднее'});

const groupMathematicians = [
    new Student({name: 'Габуниа Вася', sex: 'м', age: 22, education: 'Среднее'}),
    new Student({name: 'Марченкова Екатерина', sex: 'ж', age: 22, education: 'Среднее'}),
    new Student({name: 'Свиридов Владимир', sex: 'м', age: 22, education: 'Среднее'}),
    st1,
    new Student({name: 'Зюзько Станислав', sex: 'м', age: 22, education: 'Среднее'})
]

const prof1 = new Prof({name: 'Заманюк Иван Антонович', sex: 'м', age: 45, courseName: 'Высшая математика'});
const prof2 = new Prof({name: 'Коровченко Игорь Сергеевич', sex: 'м', age: 30, courseName: 'Программирование'});
const prof3 = new Prof({name: 'Брезгунова Виктория Сергеевна', sex: 'ж', age: 29, courseName: 'История'});
const prof4 = new Prof({name: 'Туленко Елена Владимировна', sex: 'ж', age: 35, courseName: 'Теория вероятности'});
const prof5 = new Prof({name: 'Земской Кирилл Артемович', sex: 'м', age: 50, courseName: 'Криптография'});
const prof6 = new Prof({name: 'Провоторов Максим Игоревич', sex: 'м', age: 30, courseName: 'Молекулярная физика'});

const groups = [
    new Group({name: 'Информатика и вычислительна техника 03.06.04', curator: prof2, students: groupPhysicists}),
    new Group({name: 'Математика 01.05.11', curator: prof1, students: groupMathematicians})
];

const mathFaculty = new Faculty({
    name: 'Математический',
    dean: prof4,
    groups: groups,
    professors: [prof1, prof2, prof3, prof6, prof5, prof4]
});

console.log(`На нашем факультете ${mathFaculty.studentsCount} студентов`);
st1.protectionOfTheDiploma();
console.log(groups[1].studentsString);
console.log(groups[0].studentsString);
mathFaculty.dismiss('Земской Кирилл Артемович');
mathFaculty.expulsion('Габуниа Вася', 'Математика 01.05.11');
