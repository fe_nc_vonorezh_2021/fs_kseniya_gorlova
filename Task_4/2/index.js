function getUserName(str) {
    if (!str) {
        return str;
    }
    return str[0].toUpperCase() + str.slice(1);
}

function printWelcome() {
    const nameUser = prompt('Как тебя зовут?');
    let ageUser = parseInt(prompt('Сколько тебе лет?'));

    while (ageUser < 0 || isNaN(ageUser)) {
        alert('Введите корректный возраст');
        ageUser = parseInt(prompt('Сколько тебе лет?'));
    }

    alert(`Привет, ${getUserName(nameUser)}, тебе уже ${ageUser} лет`);
}

printWelcome();
