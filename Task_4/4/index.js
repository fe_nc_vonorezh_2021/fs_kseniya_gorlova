function fizzBuzz(start, stop) {
    const gameArray = [];
    for (let i = start; i <= stop; i++) {
        if (i % 15 === 0) {
            gameArray.push('FizzBuzz');
        } else if (i % 3 === 0) {
            gameArray.push('Fizz');
        } else if (i % 5 === 0) {
            gameArray.push('Buzz');
        } else {
            gameArray.push(i);
        }
    }
    document.getElementById('item').innerHTML = `<h3>${gameArray.join(', ')}</h3>`;
}

fizzBuzz(1, 100);
