function swap(a, b) {
    let tmp = a;
    a = b;
    b = tmp;
    return [a, b]
}

function swapNoTmp(a, b) {
    return [b, a]
}

function check(a, b, callback) {
    alert(`Перед перестановки а = ${a}, b = ${b}`);
    const swapArr = callback(a, b);
    alert(`После перестановки а = ${swapArr[0]}, b = ${swapArr[1]}`);
}

const num1 = 5;
const num2 = 10;

check(num1, num2, swap);
check(num1, num2, swapNoTmp);
