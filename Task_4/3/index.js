function getRandomArr() {
    return [...Array(20)].map(() => Math.floor(Math.random() * 101));
}

function sort(arr, option) {
    return option && option.toLowerCase() === 'desc' ? [...arr].sort((a, b) =>  b - a) : [...arr].sort((a, b) =>  a - b);
}

//2 пункт задания
function sumOdd(arr) {
    document.getElementById('sum').innerHTML = arr.reduce((sum, item) => item % 2 !== 0 ? sum + item * item : sum, 0);
}

const randomArr = getRandomArr();

document.getElementById('arr').innerHTML = randomArr.toString();
document.getElementById('ascSort').innerHTML = sort(randomArr, 'asc');
document.getElementById('descSort').innerHTML = sort(randomArr, 'desc');

sumOdd(randomArr);
