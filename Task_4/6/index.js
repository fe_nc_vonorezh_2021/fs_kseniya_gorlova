function findNumber() {
    const cell = Math.floor(Math.random() * 1001);
    let count = 1;
    let number = getNumber();

    while (cell !== number) {
        if (number <= 0 || !number) {
            count += 1;
            number = getNumber();
        } else if (number < cell) {
            count += 1;
            alert('Искомое число больше!');
            number = getNumber();
        } else if (number > cell) {
            count += 1;
            alert('Искомое число меньше!');
            number = getNumber();
        }
    }

    if (confirm(`Вы угадали! Количество попыток: ${count}. Начать заново?`)) {
        findNumber();
    } else {
        alert('Спасибо за игру!')
    }
}

function getNumber() {
    return parseInt(prompt('Введите число'));
}
