import {Student, StudentProps} from "./Student";
import {Professor, ProfessorProps} from "./Professor";
import {Faculty} from "./Faculty";
import {PersonFabric} from "./PersonFabric";
import {Group, GroupProps} from "./Group";

const fabric = new PersonFabric();

const groupPhysicists = ([
    {name: 'Горлова Ксения', sex: 'ж', age: 22, education: 'Среднее'},
    {name: 'Сидоров Иван', sex: 'м', age: 21, education: 'Среднее'},
    {name: 'Гаврилов Степан', sex: 'м', age: 22, education: 'Среднее'},
    {name: 'Соболева Валентина', sex: 'ж', age: 21, education: 'Среднее'},
    {name: 'Смирнов Александр', sex: 'м', age: 21, education: 'Среднее'}
] as StudentProps[]).map(props => fabric.create<Student, StudentProps>(Student, props));

const groupMathematicians = ([
    {name: 'Габуниа Вася', sex: 'м', age: 22, education: 'Среднее'},
    {name: 'Марченкова Екатерина', sex: 'ж', age: 22, education: 'Среднее'},
    {name: 'Свиридов Владимир', sex: 'м', age: 22, education: 'Среднее'},
    {name: 'Зюзько Станислав', sex: 'м', age: 22, education: 'Среднее'},
    {name: 'Сидоренко Алевтина', sex: 'ж', age: 22, education: 'Среднее'}
] as StudentProps[]).map(props => fabric.create<Student, StudentProps>(Student, props));


const prof1 = fabric.create<Professor, ProfessorProps>(Professor, {name: 'Заманюк Иван Антонович', sex: 'м', age: 45, courseName: 'Высшая математика'});
const prof2 = fabric.create<Professor, ProfessorProps>(Professor,{name: 'Коровченко Игорь Сергеевич', sex: 'м', age: 30, courseName: 'Программирование'});
const prof3 = fabric.create<Professor, ProfessorProps>(Professor,{name: 'Брезгунова Виктория Сергеевна', sex: 'ж', age: 29, courseName: 'История'});
const prof4 = fabric.create<Professor, ProfessorProps>(Professor,{name: 'Туленко Елена Владимировна', sex: 'ж', age: 35, courseName: 'Теория вероятности'});
const prof5 = fabric.create<Professor, ProfessorProps>(Professor,{name: 'Земской Кирилл Артемович', sex: 'м', age: 50, courseName: 'Криптография'});
const prof6 = fabric.create<Professor, ProfessorProps>(Professor,{name: 'Провоторов Максим Игоревич', sex: 'м', age: 30, courseName: 'Молекулярная физика'});

const groups = ([
    {name: 'Информатика и вычислительна техника 03.06.04', curator: prof2, students: groupPhysicists},
    {name: 'Математика 01.05.11', curator: prof1, students: groupMathematicians}
] as GroupProps[]).map(props => fabric.create<Group, GroupProps>(Group, props));

const mathFaculty = new Faculty({
    name: 'Математический',
    dean: prof4,
    groups: groups,
    professors: [prof1, prof2, prof3, prof6, prof5, prof4]
});

console.log(`На нашем факультете ${mathFaculty.studentsCount} студентов`);
console.log(`На нашем факультете работают профессора: ${mathFaculty.professors}`);
groupMathematicians.find(student => student.name === 'Сидоренко Алевтина').protectionOfTheDiploma();
console.log(groups[1].studentsString);
console.log(groups[0].studentsString);
mathFaculty.dismiss('Земской Кирилл Артемович');
mathFaculty.expulsion('Габуниа Вася', 'Математика 01.05.11');
