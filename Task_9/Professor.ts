import {Person, PersonProps} from "./Person";

export interface ProfessorProps extends PersonProps {
    readonly courseName: string;
}

export class Professor extends Person {
    private _courseName: string;

    constructor(person: ProfessorProps) {
        super(person);
        this._courseName = person.courseName;
    }

    get courseName(): string {
        return this._courseName;
    }

    set courseName(newCourseName: string) {
        this._courseName = newCourseName;
    }

    teachStudents() {
        console.log(`${this.name} учит студентов предмету ${this._courseName}...`)
    }
}
