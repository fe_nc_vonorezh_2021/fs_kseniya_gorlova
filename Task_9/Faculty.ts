import {Group} from "./Group";
import {Professor} from "./Professor";

export interface FacultyProps {
    name: string;
    dean: Professor;
    groups: Array<Group>;
    professors: Array<Professor>;
}

export class Faculty {
    private _name: string;
    private readonly _dean: Professor;
    private _groups: Array<Group>;
    private _professors: Array<Professor>;

    constructor({name, dean, groups, professors}: FacultyProps) {
        this._name = name;
        this._dean = dean;
        this._groups = groups;
        this._professors = professors;
    }

    get name(): string {
        return this._name;
    }

    set name(newName: string) {
        this._name = newName;
    }

    get studentsCount(): number {
        return this._groups.reduce((sum: number, group: Group) => {
            return sum + group.students.length
        }, 0)
    }

    set professors(newProfessor: Array<Professor>) {
        this._professors = newProfessor;
    }

    get professors(): Array<Professor> {
        return this._professors;
    }

    get groups(): Array<Group> {
        return this._groups;
    }

    set groups(newGroups: Array<Group>) {
        this._groups = newGroups;
    }

    dismiss(nameProfessor: string) {
        console.log(`Профессор ${nameProfessor} был уволен деканом ${this._dean}`);
        this.professors = this.professors.filter(prof => prof.name !== nameProfessor);
    }

    expulsion(studentName: string, groupName: string) {
        console.log(`Студент ${studentName} был отчислен деканом ${this._dean}`);
        const group = this.groups.find(group => group.name === groupName);
        group.students = group.students.filter(student => student.name !== studentName);
    }
}