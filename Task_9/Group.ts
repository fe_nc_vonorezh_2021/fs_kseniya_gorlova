import {Student} from "./Student";
import {Professor} from "./Professor";

export interface GroupProps {
    name: string;
    curator: Professor;
    students: Array<Student>;
}

export class Group {
    private _name: string;
    private _curator: Professor;
    private _students: Array<Student>;

    constructor({name, curator, students}: GroupProps) {
        this._name = name;
        this._curator = curator;
        this._students = students;
    }

    get name() {
        return this._name;
    }

    set name(newName: string) {
        this._name = newName;
    }

    get curator(): Professor {
        return this._curator;
    }

    set curator(newCurator: Professor) {
        this._curator = newCurator;
    }

    get students(): Array<Student> {
        return this._students;
    }

    set students(newStudents: Array<Student>) {
        this._students = newStudents;
    }

    get studentsString() {
        return this.students.reduce((res, student) => {
            return `${res}\n${student.toString()}`
        }, '')
    }
}
