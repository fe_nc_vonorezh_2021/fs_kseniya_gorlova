import {Person, PersonProps} from "./Person";

export interface StudentProps extends PersonProps {
    readonly education: string
}

export class Student extends Person {
    private _education: string;

    constructor(person: StudentProps) {
        super(person);
        this._education = person.education;
    }

    set education(newEducation: string) {
        this._education = newEducation;
    }

    get education(): string {
        return this._education;
    }

    protectionOfTheDiploma() {
        this._education = 'Высшее';
    }

    toString(): string {
        return `${this.name}, пол: ${this.sex}, образование: ${this.education}`
    }
}
