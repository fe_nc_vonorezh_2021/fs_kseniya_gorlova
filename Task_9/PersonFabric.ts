const log = (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    const originalMethod = descriptor.value;
    descriptor.value = function(...args: any[]){
        console.log(`Object ${args[0].name} created!`);
        return originalMethod.apply(this, args);
    }
}

export class PersonFabric {

    @log
    create<T, S>(type: { new(props: S): T; }, props: S): T {
        return new type(props);
    }

}




