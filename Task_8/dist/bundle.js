/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./0/index.js":
/*!********************!*\
  !*** ./0/index.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ swap)\n/* harmony export */ });\nfunction swapWithTmp(a, b) {\r\n    let tmp = a;\r\n    a = b;\r\n    b = tmp;\r\n    return [a, b]\r\n}\r\n\r\nfunction swapNoTmp(a, b) {\r\n    return [b, a]\r\n}\r\n\r\nfunction check(a, b, callback) {\r\n    alert(`Перед перестановки а = ${a}, b = ${b}`);\r\n    const swapArr = callback(a, b);\r\n    alert(`После перестановки а = ${swapArr[0]}, b = ${swapArr[1]}`);\r\n}\r\n\r\nfunction swap() {\r\n    const num1 = 5;\r\n    const num2 = 10;\r\n\r\n    check(num1, num2, swapWithTmp);\r\n    check(num1, num2, swapNoTmp);\r\n}\r\n\n\n//# sourceURL=webpack://fs_kseniya_gorlova/./0/index.js?");

/***/ }),

/***/ "./1/index.js":
/*!********************!*\
  !*** ./1/index.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ welcome)\n/* harmony export */ });\nfunction welcome() {\r\n    alert(\"Hello JavaScript!\");\r\n}\r\n\n\n//# sourceURL=webpack://fs_kseniya_gorlova/./1/index.js?");

/***/ }),

/***/ "./2/index.js":
/*!********************!*\
  !*** ./2/index.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ printWelcome)\n/* harmony export */ });\nfunction getUserName(str) {\r\n    if (!str) {\r\n        return str;\r\n    }\r\n    return str[0].toUpperCase() + str.slice(1);\r\n}\r\n\r\nfunction printWelcome() {\r\n    const nameUser = prompt('Как тебя зовут?');\r\n    let ageUser = parseInt(prompt('Сколько тебе лет?'));\r\n\r\n    while (ageUser < 0 || isNaN(ageUser)) {\r\n        alert('Введите корректный возраст');\r\n        ageUser = parseInt(prompt('Сколько тебе лет?'));\r\n    }\r\n\r\n    alert(`Привет, ${getUserName(nameUser)}, тебе уже ${ageUser} лет`);\r\n}\r\n\r\nprintWelcome();\r\n\n\n//# sourceURL=webpack://fs_kseniya_gorlova/./2/index.js?");

/***/ }),

/***/ "./3/index.js":
/*!********************!*\
  !*** ./3/index.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ logic)\n/* harmony export */ });\nfunction getRandomArr() {\r\n    return [...Array(20)].map(() => Math.floor(Math.random() * 101));\r\n}\r\n\r\nfunction sort(arr, option) {\r\n    return option && option.toLowerCase() === 'desc' ? [...arr].sort((a, b) =>  b - a) : [...arr].sort((a, b) =>  a - b);\r\n}\r\n\r\n//2 пункт задания\r\nfunction sumOdd(arr) {\r\n    document.getElementById('sum').innerHTML = arr.reduce((sum, item) => item % 2 !== 0 ? sum + item * item : sum, 0);\r\n}\r\n\r\nfunction logic() {\r\n    const randomArr = getRandomArr();\r\n\r\n    document.getElementById('arr').innerHTML = randomArr.toString();\r\n    document.getElementById('ascSort').innerHTML = sort(randomArr, 'asc');\r\n    document.getElementById('descSort').innerHTML = sort(randomArr, 'desc');\r\n\r\n    sumOdd(randomArr);\r\n}\n\n//# sourceURL=webpack://fs_kseniya_gorlova/./3/index.js?");

/***/ }),

/***/ "./4/index.js":
/*!********************!*\
  !*** ./4/index.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ printFizzBuzz)\n/* harmony export */ });\nfunction fizzBuzz(start, stop) {\r\n    const gameArray = [];\r\n    for (let i = start; i <= stop; i++) {\r\n        if (i % 15 === 0) {\r\n            gameArray.push('FizzBuzz');\r\n        } else if (i % 3 === 0) {\r\n            gameArray.push('Fizz');\r\n        } else if (i % 5 === 0) {\r\n            gameArray.push('Buzz');\r\n        } else {\r\n            gameArray.push(i);\r\n        }\r\n    }\r\n    document.getElementById('item').innerHTML = `<h3>${gameArray.join(', ')}</h3>`;\r\n}\r\n\r\nfunction printFizzBuzz() {\r\n    fizzBuzz(1, 100);\r\n}\r\n\n\n//# sourceURL=webpack://fs_kseniya_gorlova/./4/index.js?");

/***/ }),

/***/ "./5/index.js":
/*!********************!*\
  !*** ./5/index.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ printPalindrome)\n/* harmony export */ });\nfunction palindrome() {\r\n    let word = prompt('Введите ваше слово: ');\r\n    let tmp = word.split('');\r\n    if (tmp.reverse().join('').toLowerCase() === word.toString().toLowerCase()) {\r\n        alert(`${word} это палиндром!`)\r\n    } else {\r\n        alert(`${word} не палиндром!`)\r\n    }\r\n}\r\n\r\nfunction printPalindrome() {\r\n    palindrome();\r\n}\r\n\n\n//# sourceURL=webpack://fs_kseniya_gorlova/./5/index.js?");

/***/ }),

/***/ "./6/index.js":
/*!********************!*\
  !*** ./6/index.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ findNumber)\n/* harmony export */ });\nfunction findNumber() {\r\n    const cell = Math.floor(Math.random() * 1001);\r\n    let count = 1;\r\n    let number = getNumber();\r\n\r\n    while (cell !== number) {\r\n        if (number <= 0 || !number) {\r\n            count += 1;\r\n            number = getNumber();\r\n        } else if (number < cell) {\r\n            count += 1;\r\n            alert('Искомое число больше!');\r\n            number = getNumber();\r\n        } else if (number > cell) {\r\n            count += 1;\r\n            alert('Искомое число меньше!');\r\n            number = getNumber();\r\n        }\r\n    }\r\n\r\n    if (confirm(`Вы угадали! Количество попыток: ${count}. Начать заново?`)) {\r\n        findNumber();\r\n    } else {\r\n        alert('Спасибо за игру!')\r\n    }\r\n}\r\n\r\nfunction getNumber() {\r\n    return parseInt(prompt('Введите число'));\r\n}\r\n\n\n//# sourceURL=webpack://fs_kseniya_gorlova/./6/index.js?");

/***/ }),

/***/ "./main.js":
/*!*****************!*\
  !*** ./main.js ***!
  \*****************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./0 */ \"./0/index.js\");\n/* harmony import */ var _1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./1 */ \"./1/index.js\");\n/* harmony import */ var _2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./2 */ \"./2/index.js\");\n/* harmony import */ var _3__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./3 */ \"./3/index.js\");\n/* harmony import */ var _4__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./4 */ \"./4/index.js\");\n/* harmony import */ var _5__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./5 */ \"./5/index.js\");\n/* harmony import */ var _6__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./6 */ \"./6/index.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\n\n//# sourceURL=webpack://fs_kseniya_gorlova/./main.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./main.js");
/******/ 	
/******/ })()
;