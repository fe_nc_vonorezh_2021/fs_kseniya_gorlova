const {src, dest} = require('gulp');
const del = require('del');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

function js() {
    return src('./*/**.js')
        .pipe(concat('index.js'))
        .pipe(uglify())
        .pipe(dest('dist'))
}

function clear() {
    return del('dist')
}

exports.js = js;
exports.clear = clear