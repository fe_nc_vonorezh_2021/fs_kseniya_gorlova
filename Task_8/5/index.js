function palindrome() {
    let word = prompt('Введите ваше слово: ');
    let tmp = word.split('');
    if (tmp.reverse().join('').toLowerCase() === word.toString().toLowerCase()) {
        alert(`${word} это палиндром!`)
    } else {
        alert(`${word} не палиндром!`)
    }
}

export default function printPalindrome() {
    palindrome();
}
