import swap from "./0";
import welcome from "./1";
import printWelcome from "./2";
import logic from "./3";
import printFizzBuzz from "./4";
import printPalindrome from "./5";
import findNumber from "./6";
