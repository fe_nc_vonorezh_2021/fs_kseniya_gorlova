const regExpWhiteSpace = /^\s+/;
const regExpIsEmptyLine = /^\s*$/;
const btnAddBook = document.querySelector('.btn-add-book');
const name = document.querySelector('.name');
const author = document.querySelector('.author');
const genre = document.querySelector('.genre');
const publishingHouse = document.querySelector('.publishing-house');
const save = document.querySelector('.btn-save');
const tableBody = document.querySelector('.table tbody');
const form = document.querySelector('.book-form');
const error = document.querySelector('.error span');
let lastId = 1;

btnAddBook.addEventListener('click', e => {
    e.preventDefault();

    if (!isValid()) {
        error.textContent = "Заполните все поля";
    } else {
        const newBook = {
            name: name.value,
            author: author.value,
            genre: genre.value,
            publishingHouse: publishingHouse.value,
        };

        tableBody.insertAdjacentHTML('beforeend', getNewRow(newBook));
        error.textContent = "";
        form.reset();
    }
})

save.addEventListener('click', event => saveChanges(event));

function getNewRow({name, author, genre, publishingHouse}) {
    const id = ++lastId;
    return `
    <tr id="book-${id}">
        <td>${id}</td>
        <td>${name}</td>
        <td>${author}</td>
        <td>${genre}</td>
        <td>${publishingHouse}</td>
        <td class="buttons">
            <span><img class="edit" alt="img edit" src="img/edit.png" onclick="editBook(this)"/></span>
            <span><img class="delete" alt="img delete" src="img/delete.png" onclick="deleteBook(this)"/></span>
        </td>
    </tr>`;
}

function deleteBook(element) {
    const tr = element.closest('tr');
    const nameBook = tr.querySelector('td:nth-child(2)').textContent;

    if (confirm(`Вы уверены, что хотите удалить книгу "${nameBook}"?`)) {
        tr.parentNode.removeChild(tr);
    }
}

function editBook(element) {
    document.querySelectorAll('tr.editing').forEach(tr => tr.classList.remove('editing'));
    btnAddBook.classList.add('none');
    save.classList.remove('none');
    const tr = element.closest('tr');
    tr.classList.add('editing');

    const nameBook = tr.querySelector('td:nth-child(2)').textContent;
    const authorBook = tr.querySelector('td:nth-child(3)').textContent;
    const genreBook = tr.querySelector('td:nth-child(4)').textContent;
    const publishingHouseBook = tr.querySelector('td:nth-child(5)').textContent;

    name.value = nameBook;
    author.value = authorBook;
    genre.value = genreBook;
    publishingHouse.value = publishingHouseBook;
}

function saveChanges(event) {
    event.preventDefault();
    const tr = document.querySelector('tr.editing');
    tr.querySelector('td:nth-child(2)').textContent = name.value;
    tr.querySelector('td:nth-child(3)').textContent = author.value;
    tr.querySelector('td:nth-child(4)').textContent = genre.value;
    tr.querySelector('td:nth-child(5)').textContent = publishingHouse.value;
    tr.classList.remove('editing');

    form.reset();
    save.classList.add('none');
    btnAddBook.classList.remove('none');
}

function isValid() {
    let hasError = false;
    document.querySelectorAll('.field input').forEach(input => {
        const hasEmptyFields = regExpIsEmptyLine.test(input.value) || regExpWhiteSpace.test(input.value);

        if (hasEmptyFields) {
            input.classList.add('invalid');
            hasError = true;
        } else {
            input.classList.remove('invalid');
        }
    })

    return !hasError;
}
